<?php

    require_once("./class/Cadastra.class.php");

    if($_SERVER['REQUEST_METHOD']== "POST" && count($_POST) == 8) {

        /*
            Propriedades esperadas na requisição:
            Nome
            Email
            CEP
            Estado
            Cidade
            Endereco
            Data
            Sexo
        */

         $novaQuery = [];

        if(filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING)) {
            $novaQuery["nome"] = filter_input(INPUT_POST , 'nome', FILTER_SANITIZE_STRING);
        }
        if(filter_input(INPUT_POST , 'email', FILTER_VALIDATE_EMAIL)) {
            $novaQuery["email"] = filter_input(INPUT_POST , 'email', FILTER_VALIDATE_EMAIL);
        }
        if(preg_match('/^[0-9]{5}-?[0-9]{3}$/', $_POST['cep'])) {
            $novaQuery["cep"] = filter_input(INPUT_POST , 'cep', FILTER_SANITIZE_STRING);
        }
        if(filter_input(INPUT_POST, 'estado', FILTER_SANITIZE_STRING)) {
            $novaQuery["estado"] = filter_input(INPUT_POST , 'estado', FILTER_SANITIZE_STRING);
        }
        if(filter_input(INPUT_POST, 'cidade', FILTER_SANITIZE_STRING)) {
            $novaQuery["cidade"] = filter_input(INPUT_POST , 'cidade', FILTER_SANITIZE_STRING);
        }
        if(filter_input(INPUT_POST, 'endereco', FILTER_SANITIZE_STRING)) {
            $novaQuery["endereco"] = filter_input(INPUT_POST , 'endereco', FILTER_SANITIZE_STRING);
        }
        if(filter_input(INPUT_POST, 'data', FILTER_SANITIZE_STRING)) {

            //cria um array
            $array = explode('-', $_POST['data']);

            //garante que o array possue tres elementos (ano, mês e dia). A data não vem no padrão brasileiro, por isso ela é manipulada em uma ordem diferente.
            if(count($array) == 3){
                $dia = (int)$array[2];
                $mes = (int)$array[1];
                $ano = (int)$array[0];

                //testa se a data é válida
                if(checkdate($mes, $dia, $ano)){
                    $novaQuery["data"] = $dia."/".$mes."/".$ano;
                }
            }

        }

        if(filter_input(INPUT_POST, 'sexo', FILTER_SANITIZE_STRING)) {
            $novaQuery["sexo"] = filter_input(INPUT_POST , 'sexo', FILTER_SANITIZE_STRING);
        }

        if(count($novaQuery) == 8) {
            $cadastro = new \classes\Cadastra;
            echo $cadastro->cadastraCliente($novaQuery);
        }else {
            echo "Existem campos inválidos. <br>";
        }

    }

    
