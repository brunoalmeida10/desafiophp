<!DOCTYPE html>
<html lang="pt-BR">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- BIBLIOTECAS CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Cadastro simples de cliente</title>
  </head>
  <body>
    <main class="container">
      <form>

        <div class="" id="alerta" role="alert"></div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputNome">Nome completo*</label>
            <input type="text" class="form-control" id="inputNome" placeholder="Nome completo">
          </div>
          <div class="form-group col-md-6">
            <label for="inputEmail">Email*</label>
            <input type="email" class="form-control" id="inputEmail" placeholder="Email">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputCEP">CEP*</label>
            <input type="text" class="form-control" id="inputCEP" placeholder="Ex: 3500-000">
          </div>
          <div class="form-group col-md-6">
            <label for="inputEstado">Estado*</label>
            <select id="inputEstado" class="form-control">
              <option value="" disabled selected>Escolher...</option>
              <option value="Acre">Acre</option>
              <option value="Alagoas">Alagoas</option>
              <option value="Amazonas">Amazonas</option>
              <option value="Amapá">Amapá</option>
              <option value="Bahia">Bahia</option>
              <option value="Ceará">Ceará</option>
              <option value="Distrito Federal">Distrito Federal</option>
              <option value="Espírito Santo">Espírito Santo</option>
              <option value="Goiás">Goiás</option>
              <option value="Maranhão">Maranhão</option>
              <option value="Minas Gerais">Minas Gerais</option>
              <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
              <option value="Mato Grosso">Mato Grosso</option>
              <option value="Pará">Pará</option>
              <option value="Paraíba">Paraíba</option>
              <option value="Pernambuco">Pernambuco</option>
              <option value="Piauí">Piauí</option>
              <option value="Paraná">Paraná</option>
              <option value="Rio de Janeiro">Rio de Janeiro</option>
              <option value="Rio Grande do Norte">Rio Grande do Norte</option>
              <option value="Rondônia">Rondônia</option>
              <option value="Roraima">Roraima</option>
              <option value="Rio Grande do Sul">Rio Grande do Sul</option>
              <option value="Santa Catarina">Santa Catarina</option>
              <option value="Sergipe">Sergipe</option>
              <option value="São Paulo">São Paulo</option>
              <option value="Tocantins">Tocantins</option>
            </select>
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputCidade">Cidade*</label>
            <input type="text" class="form-control" id="inputCidade" placeholder="Cidade">
          </div>
          <div class="form-group col md-6">
            <label for="inputEndereco">Endereço*</label>
            <input type="text" class="form-control" id="inputEndereco" placeholder="Ex: Rua Marechal Deodoro, nº 100">
          </div>
        </div>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputData">Data de nascimento*</label>
            <input type="date" class="form-control" id="inputData">
          </div>
          <div class="form-group col-md-6">
            <label for="inputSexo">Sexo*</label>
            <select id="inputSexo" class="form-control">
                <option value="" selected disabled>Sexo...</option>
              <option value="masculino">Masculino</option>
              <option value="femino">Feminino</option>
              <option value="outros">Outros</option>
            </select>
          </div>
        </div>
        <button type="button" class="btn btn-primary" id="cadastrar">Cadastrar</button>
      </form>
    </main>


    <script>

        document.getElementById("cadastrar").addEventListener("click", validaFormulario)

        async function validaFormulario() {

            //Valida campo de nome
            if(document.querySelector("#inputNome").value.trim().split(" ").length < 2) {
                document.getElementById("alerta").innerHTML = "Por favor, escreva seu nome completo."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            //Valida campo de e-mail
            else if(/\S+@\S+\.\S+/.test(document.querySelector("#inputEmail").value.trim()) == false) {
                document.getElementById("alerta").innerHTML = "Por favor, digite um e-mail válido."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            //Valida campo de CEP
            else if(/^[0-9]{5}-?[0-9]{3}$/.test(document.querySelector("#inputCEP").value.trim()) == false) {
                document.getElementById("alerta").innerHTML = "Por favor, digite um CEP válido."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            //Valida campo de Estado
            else if(document.querySelector("#inputEstado").value.trim().length < 4) {
                document.getElementById("alerta").innerHTML = "Por favor, selecione um estado."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            //Valida campo de Cidade
            else if(document.querySelector("#inputCidade").value.trim().length < 4) {
                document.getElementById("alerta").innerHTML = "Por favor, digite a cidade."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            //Valida campo de Endereço
            else if(document.querySelector("#inputEndereco").value.trim().length < 4) {
                document.getElementById("alerta").innerHTML = "Por favor, digite o endereço."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            //Valida campo da data de nascimento
            else if(new Date() > new Date(document.querySelector("#inputData").value.trim()) == false) {
                document.getElementById("alerta").innerHTML = "Por favor, digite uma data de nascimento válida."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            //Valida campo de sexo
            else if(document.getElementById("inputSexo").value.trim().length < 6) {
                document.getElementById("alerta").innerHTML = "Por favor, selecione um sexo."
                document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                return;
            }
            else {
                /*
                    O ideal é realizar a quebra de linha na montagem da query (em desenvolvimento), a fim de impedir que a linha fique muito longa.
                    Deixei tudo na mesma linha para poupar tempo na validação, pois a requisição iria ser enviada com vários espaçamentos entre as propriedades da query.
                */
                var query = `nome=${document.querySelector("#inputNome").value.trim()}&email=${document.querySelector("#inputEmail").value.trim()}&cep=${document.querySelector("#inputCEP").value.trim()}&estado=${document.querySelector("#inputEstado").value.trim()}&cidade=${document.querySelector("#inputCidade").value.trim()}&endereco=${document.querySelector("#inputEndereco").value.trim()}&data=${document.querySelector("#inputData").value.trim()}&sexo=${document.querySelector("#inputSexo").value.trim()}`;

                var requisicao = await cadastraCliente(query.trim());

                if(requisicao == "true") {
                    document.getElementById("alerta").innerHTML = "Cliente cadastrado com sucesso!."
                    document.getElementById("alerta").setAttribute("class", "alert alert-success")
                }else {
                    document.getElementById("alerta").innerHTML = requisicao
                    document.getElementById("alerta").setAttribute("class", "alert alert-danger")
                }
            }
        }

        async function cadastraCliente(query) {
            var requisicao = await fetch("./cadastra.php", {
                method: "post",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                body: query
            });
            var resposta   = await requisicao.text();
            return resposta;
        }

    </script>


  </body>
</html>