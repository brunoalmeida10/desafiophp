<?php

    namespace classes;

    class Cadastra {

        private $arquivoClientes = "clientes.csv";

        public function verificaExistenciaCliente($email):bool {

            $arquivo = file_get_contents($this->arquivoClientes);
            if (strpos($arquivo, $email) !== FALSE) {
                //Já existe um e-mail parecido cadastrado
                return true;
            }else {
                //Não existe um e-mail parecido cadastrado
                return false;
            }

        }

        public function cadastraCliente($dados):string {

            if(Self::verificaExistenciaCliente($dados["email"])) {
                return "E-mail já cadastrado!";
            }else {
             $fp = fopen($this->arquivoClientes, 'a+');
             if(!fputcsv($fp, $dados))
                die("Erro ao cadastrar cliente.");
             fclose($fp);
                return "true";
            }

        }

    }